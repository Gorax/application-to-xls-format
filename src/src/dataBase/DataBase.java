package src.dataBase;

import src.model.FileWithDescription;

import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class DataBase {
    private List<FileWithDescription> fileList = new ArrayList<>();

    public List<FileWithDescription> getFileList() {
        return fileList;
    }
}
