package src.model;

import java.io.File;

public class FileWithDescription {

    private File file;
    private String description;

    public FileWithDescription(File file, String description) {
        this.file = file;
        this.description = description;
    }

    public File getFile() {
        return file;
    }

    public String getDescription() {
        return description;
    }
}
