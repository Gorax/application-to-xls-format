package src.controllers;

import src.Service.OperationService;
import src.dataBase.DataBase;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/file")
public class FileController extends HttpServlet {

    @EJB
    private DataBase dataBase;

    @EJB
    private OperationService operationService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!check(req, resp))
            resp.sendRedirect(resp.encodeRedirectURL("file.jsp?notFound"));
    }

    private boolean check(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        for (int i = 0; i < dataBase.getFileList().size(); i++) {
            if (req.getParameter("" + i) != null) {
                req.setAttribute("description", dataBase.getFileList().get(i).getDescription());
                req.setAttribute("index", "" + i);
                req.getRequestDispatcher("file.jsp").forward(req, resp);
                return true;
            } else if (req.getParameter("delete" + i) != null) {
                operationService.delete(i);
                resp.sendRedirect(resp.encodeRedirectURL("allFiles"));
                return true;
            } else if (req.getParameter("download" + i) != null) {
                if (operationService.downloadFile(i)) {
                    req.setAttribute("description", "The file has been properly downloaded to C:Download");
                    req.getRequestDispatcher("file.jsp").forward(req, resp);
                    return true;
                }
            }
        }
        return false;
    }
}
