package src.controllers;

import src.dataBase.DataBase;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/allFiles")
public class AllFilesController extends HttpServlet {

    @EJB
    private DataBase dataBase;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("fileList", dataBase.getFileList());
        req.getRequestDispatcher("allFiles.jsp").forward(req, resp);
    }
}
