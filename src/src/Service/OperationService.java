package src.Service;

import src.dataBase.DataBase;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.io.*;
import java.nio.file.Files;

@Stateless
public class OperationService {

    @EJB
    private DataBase dataBase;

    private OutputStream outputStream = null;

    public void delete(int index) {
        dataBase.getFileList().remove(index);
    }

    public boolean downloadFile(int index) {
        File file = dataBase.getFileList().get(index).getFile();
        try {
            byte[] fileToWrite = Files.readAllBytes(file.toPath());
            outputStream = new FileOutputStream("C:\\Download\\" + file.getName());
            outputStream.write(fileToWrite);
            outputStream.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
