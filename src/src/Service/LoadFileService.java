package src.Service;

import src.dataBase.DataBase;
import src.model.FileWithDescription;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.io.File;

@Stateless
public class LoadFileService {

    @EJB
    private DataBase dataBase;

    public boolean addFile(File file, String description) {
        if (validation(file, description)) {
            dataBase.getFileList().add(new FileWithDescription(file, description));
            return true;
        } else {
            return false;
        }
    }

    private boolean validation(File file, String description) {
        if (file != null && file.getName().endsWith(".xls") && description != null && !description.isEmpty()) {
            return true;
        }
        return false;
    }
}
