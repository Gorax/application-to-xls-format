<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Application to xls format</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/shop-item.css" rel="stylesheet">
</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="home.jsp">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<!-- Page Content -->
<div class="container">
    <%
        String description = (String) request.getAttribute("description");
        String index = (String) request.getAttribute("index");
        String notFound = request.getParameter("notFound");
    %>

    <div class="row">

        <div class="col-lg-3" style="margin-top: 8%">
            <div class="list-group">
                <a class="list-group-item">Choose one of the following actions:</a>
            </div>

            <div class="list-group">
                <a href="home" class="list-group-item">Load file</a>
            </div>

            <div class="list-group">
                <a href="allFiles" class="list-group-item">Show all files</a>
            </div>

            <div class="list-group">
                <a class="list-group-item active">File view</a>
            </div>

            <div class="list-group">
                <a href="file?delete<%=index%>" class="list-group-item">delete file</a>
            </div>

            <div class="list-group">
                <a href="file?download<%=index%>" class="list-group-item">Download file</a>
            </div>
        </div>


        <div class="col-lg-9">
            <h2 style="text-align: center; margin-top: 3%">Application to xls format</h2>
            <div class="card card-outline-secondary my-4">
                <div class="card-header">
                    Your file description:
                </div>
                <div class="card-body">

                    <%
                        if (description != null) {
                    %>
                    <a><%=description%>
                    </a>
                    <%
                    } else if (notFound != null) {
                    %>
                    <a>Something went wrong, file not found.</a>
                    <%
                    } else {
                    %>
                    <a>You did not select any file.</a>
                    <%
                        }
                    %>
                    <%--<img class="card-img-top img-fluid" src="" alt="cos poszlo nie tak">--%>
                </div>
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col-lg-9 -->
    </div>

</div>

</body>

</html>
