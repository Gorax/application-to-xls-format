<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Application to xls format</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/shop-item.css" rel="stylesheet">
</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">

    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="home">Home
                        <span class="sr-only">(current)</span>
                    </a>
            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-lg-3" style="margin-top: 8%">
            <div class="list-group">
                <a class="list-group-item">Choose one of the following actions:</a>
                <%--Choose one of the following actions:--%>
            </div>

            <div class="list-group">
                <a href="home" class="list-group-item active">Load file</a>
            </div>

            <div class="list-group">
                <a href="allFiles" class="list-group-item">Show all files</a>
            </div>
        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-9">
            <h2 style="text-align: center; margin-top: 3%">Application to xls format</h2>

            <div class="card mt-4">
                <div class="card-body">
                    <p class="card-text">Load .xls file:</p>

                    <form method="post" action="loadFile" enctype="multipart/form-data">
                        <input type="file" name="file">
                        <br>
                        <a>Tap your description:</a>
                        <br>
                        <textarea name="description" rows="3" cols="50"></textarea>
                        <br>
                        <br>
                        <input type="submit" value="Send" style="border-radius: 15px">
                    </form>

                </div>
            </div>
            <!-- /.card -->

            <div class="card card-outline-secondary my-4">
                <div class="card-header">
                    State:
                </div>
                <div class="card-body">
                    <%
                        String invalidData = request.getParameter("invalidData");
                        String success = request.getParameter("success");
                        if (invalidData != null) {
                    %>
                    <h6>The file could not be loaded. Make sure you have loaded the correct file and fill in its
                        description.
                    </h6>
                    <%
                    } else if (success != null) {
                    %>
                    <h6>File loaded correctly.
                    </h6>
                    <%
                        }
                    %>
                    <p>
                    <hr>
                </div>
            </div>
            <!-- /.card -->

        </div>
        <!-- /.col-lg-9 -->

    </div>

</div>

</body>

</html>
